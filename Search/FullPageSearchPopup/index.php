<aside class="fullsearch">
        <figure>
            <img src="<?php echo get_template_directory_uri(); ?>/dist/images/logo.png" alt="Bonsensa Logo" title="Bonsensa Logo"/>
        </figure>
        <span class="fullsearch__close icon-cancel-circle"></span>
        <?php get_search_form(); ?>
</aside>

<script>
    /****************************************
    Searchform Fullpage
    ****************************************/
    function makeSearchFull() {
        const closingButton = document.querySelector('.fullsearch__close');
        const searchScreen = document.querySelector('.fullsearch');
        const searchOpener = document.querySelector('.searchopenenr');

        closingButton.addEventListener('click', function() {
            searchScreen.classList.remove('fullsearchin');
        });
        searchOpener.addEventListener('click', function() {
            searchScreen.classList.toggle('fullsearchin');
        });

    }
</script>

<style>
    /****************************************
Fullwidth Page Stuff
****************************************/
.fullsearch {
	background-color: #fff;
	width: 100%;
	height: 100%;
	position: absolute;
	z-index: 9999999;
	top: 0;
	left: -9999px;
	display: flex;
	justify-content: center;
	align-content: center;
	flex-wrap: wrap;
	@include transImg(.7s);
	figure {
		width: 100%;
		text-align: center;
	}
	.fullsearch__close {
		position: absolute;
		right: 20px;
		top: 20px;
		font-size: 40px;
		cursor: pointer;
		&:before {
			color: $maincolor;
		}
	}

}

.fullsearchin {
	left: 0;
}

/****************************************
Searchbutton
****************************************/
.searchopenenr {
	position: absolute;
	right: 20px;
	top: 55px;
	font-size: 30px;
	cursor: pointer;
}
</style>
