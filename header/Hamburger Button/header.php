<style>
/****************************************
Mobile Button
****************************************/


.hamburger {
	@include cleanbtn;
	display: inline-block;
	width: 3rem;
	height: 3rem;
	content: "";
	margin: 0 0 -37.5% 0;
	cursor: pointer;
	background-size: 1.5rem 1.5rem;
	background-repeat: no-repeat;
	background-position: center center;
	background-image: url(data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHdpZHRoPSIzMCIgaGVpZ2h0PSIzMCIgdmlld0JveD0iMCAwIDMwIDMwIj48cGF0aCBmaWxsPSIjMzczYTNjIiBkPSJNMzAsNi4yYzAsMC43LTAuNiwxLjItMS4yLDEuMkgxLjJDMC42LDcuNSwwLDYuOSwwLDYuMlYzLjhjMC0wLjcsMC42LTEuMiwxLjItMS4yaDI3LjVjMC43LDAsMS4yLDAuNiwxLjIsMS4yIFY2LjJ6IE0zMCwxNi4yYzAsMC43LTAuNiwxLjItMS4yLDEuMkgxLjJjLTAuNywwLTEuMi0wLjYtMS4yLTEuMnYtMi41YzAtMC43LDAuNi0xLjIsMS4yLTEuMmgyNy41YzAuNywwLDEuMiwwLjYsMS4yLDEuMlYxNi4yeiBNMzAsMjYuMmMwLDAuNy0wLjYsMS4yLTEuMiwxLjJIMS4yYy0wLjcsMC0xLjItMC42LTEuMi0xLjJ2LTIuNWMwLTAuNywwLjYtMS4yLDEuMi0xLjJoMjcuNWMwLjcsMCwxLjIsMC42LDEuMiwxLjJWMjYuMnoiLz48L3N2Zz4=);
	position: absolute;
	right: 5px;
	top: 8px;
	z-index: 9999;
}
.hamburger.active {
	background-image: url(data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHdpZHRoPSIzMCIgaGVpZ2h0PSIzMCIgdmlld0JveD0iMCAwIDMwIDMwIj48cGF0aCBmaWxsPSIjMzczYTNjIiBkPSJNMTguNCwxNWw4LjEtOC4xYzAuNC0wLjQsMC41LTEuMiwwLTEuN2wtMC4xLDAuMWwtMS43LTEuN2MtMC40LTAuNC0xLjItMC41LTEuNywwbC04LDhMNywzLjUgQzYuNSwzLjEsNS44LDMsNS4zLDMuNUwzLjYsNS4yYy0wLjUsMC41LTAuNSwxLjMsMCwxLjdsOC4xLDguMUwzLjYsMjNjLTAuNCwwLjQtMC41LDEuMiwwLDEuN2wxLjcsMS43YzAuNSwwLjUsMS4zLDAuNSwxLjcsMCBsOC4xLTguMWw4LjEsOC4xYzAuNCwwLjQsMS4yLDAuNSwxLjcsMGwtMC4xLTAuMWwxLjctMS43YzAuNC0wLjQsMC41LTEuMiwwLTEuN0wxOC40LDE1eiIvPjwvc3ZnPg==)
}

@include breakpoint($mobilebreak+1 up) {
	.hamburger {
		display: none;
	}
}
</style>


<button class="menu-toggle hamburger" aria-controls="primary-menu" aria-expanded="false"><?php esc_html_e( '', 'newbonsensa' ); ?></button>