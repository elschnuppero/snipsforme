function my_theme_setup(){
    $path = get_template_directory() . '/languages';
    $result = load_theme_textdomain('my_theme', $path );
 
    if ( $result )
        return;
 
   $locale = apply_filters( 'theme_locale', get_locale(), 'my_theme' );
   die( "Could not find $path/$locale.mo." );
}