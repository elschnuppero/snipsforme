function *replaceme*_comment_moderation_recipients( $emails, $comment_id ) {
    // Adding any email
    $emails = array( '*replaceme*@*replaceme*.de' );
  
    return $emails;
}
add_filter( 'comment_moderation_recipients', '*replaceme*_comment_moderation_recipients', 11, 2 );
add_filter( 'comment_notification_recipients', '*replaceme*_comment_moderation_recipients', 11, 2 );