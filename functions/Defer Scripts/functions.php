/*
* Defer loading tag
*/
function *replaceme*_add_defer_attribute($tag, $handle) {
    // add script handles to the array below
    $scripts_to_defer = array('comment-reply', 'wp-embed');
 
    foreach($scripts_to_defer as $defer_script) {
        if ($defer_script === $handle) {
            return str_replace(' src', ' defer="defer" src', $tag);
        }
    }
    return $tag;
}
 
add_filter('script_loader_tag', '*replaceme*_add_defer_attribute', 10, 2);