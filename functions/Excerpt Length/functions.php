function *replaceme*_excerpt_length( $length ) {
    return 30;
}
add_filter( 'excerpt_length', '*replaceme*_excerpt_length', 999 );