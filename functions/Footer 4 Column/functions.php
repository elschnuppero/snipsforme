// Widgets Footer for functions.php
 
function *replaceme*_widgets_init_footer_bottom() {
    register_sidebar( array(
        'name'          => __( 'Custom Widget Area Footer_Bottom1', '*replaceme*' ),
        'id'            => 'footer-custom-widget_bottom1',
        'description'   => __( 'Custom widget area for the header of the theme', '*replaceme*' ),
        'before_widget' => '<aside id="%1$s" class="widget %2$s">',
        'after_widget'  => '</aside>',
        'before_title'  => '<h3 class="widget-title">',
        'after_title'   => '</h3>',
    ) );
    register_sidebar( array(
        'name'          => __( 'Custom Widget Area Footer_Bottom2', '*replaceme*' ),
        'id'            => 'footer-custom-widget_bottom2',
        'description'   => __( 'Custom widget area for the header of the theme', '*replaceme*' ),
        'before_widget' => '<aside id="%1$s" class="widget %2$s">',
        'after_widget'  => '</aside>',
        'before_title'  => '<h3 class="widget-title">',
        'after_title'   => '</h3>',
    ) );
 
    register_sidebar( array(
        'name'          => __( 'Custom Widget Area Footer_Bottom3', '*replaceme*' ),
        'id'            => 'footer-custom-widget_bottom3',
        'description'   => __( 'Custom widget area for the footer of the theme', '*replaceme*' ),
        'before_widget' => '<aside id="%1$s" class="widget %2$s">',
        'after_widget'  => '</aside>',
        'before_title'  => '<h3 class="widget-title">',
        'after_title'   => '</h3>',
    ) );
 
    register_sidebar( array(
        'name'          => __( 'Custom Widget Area Footer_Bottom4', '*replaceme*' ),
        'id'            => 'footer-custom-widget_bottom4',
        'description'   => __( 'Custom widget area for the footer of the theme', '*replaceme*' ),
        'before_widget' => '<aside id="%1$s" class="widget %2$s">',
        'after_widget'  => '</aside>',
        'before_title'  => '<h3 class="widget-title">',
        'after_title'   => '</h3>',
    ) );
}
add_action( 'widgets_init', '*replaceme*_widgets_init_footer_bottom' );