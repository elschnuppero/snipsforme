/**
* Replaces the excerpt "more" text by a link
**/
 
function *replaceme*_excerpt_more($more) {
    global $post;
    return '...<br><a class="moretag" href="'. get_permalink($post->ID) . '"> Lesen Sie mehr...</a>';
}
add_filter('excerpt_more', '*replaceme*_excerpt_more');