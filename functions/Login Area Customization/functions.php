/*
 * Changing the login pages URL
 */
function the_url( $url ) {
    return 'https://www.fomaco.de';
}
add_filter( 'login_headerurl', 'the_url' );
 
function my_login_logo() { ?>
    <style type="text/css">
        body.login {
            background: linear-gradient(to bottom , #fff 0%, #0D324C 100%);
        }
        .login #login {
            max-width:620px;
            width: 100%;
        }
        .login .message {
 
        }
        .login #login_error, .login .message, .login .success {
            max-width: 320px;
            margin: auto;
            text-align: center;
            display: block;
            margin-left: auto !important;
            box-shadow: 0 1px 3px rgba(0, 0, 0, 0.3) !important;
        }
        #loginform {
            max-width:320px;
            width: 100%;
            margin: auto;
            position: relative !important;
            overflow: visible;
            padding-bottom: 20px;
            background-color: #fff;
            box-shadow: 0 1px 3px rgba(0, 0, 0, 0.3) !important;
        }
        #loginform::before,
        #loginform::after {
            z-index: -1;
            position: absolute;
            content: "";
            bottom: 12px;
            left: 10px;
            width: 50%;
            top: 82%;
            max-width: 300px;
            background: rgba(255, 255, 255, 0.1);
            box-shadow: 0 15px 10px #000;
            transform: rotate(-3deg);
        }
        #loginform::after {
            transform: rotate(3deg);
            right: 10px;
            left: auto;
        }
        .login  input[type="text"],
        .login  input[type="password"]{
            padding: 13px 10px;
            width: 100%;
            font-size: 16px;
            outline: none;
            border: none;
            border-radius: 4px;
            -webkit-border-radius: 4px;
            -moz-border-radius: 4px;
            background: #eee;
        }
        .login input[type="submit"] {
            font-size: 20px;
            padding: 18px 30px !important;
            border: none;
            text-transform: capitalize;
            outline: none;
            border-radius: 4px;
            -webkit-border-radius: 4px;
            -moz-border-radius: 4px;
            background: #0D324C;
            box-shadow: 0px 3px 7px #000;
            color: #fff;
            cursor: pointer;
            margin: 30px auto 0;
            transition: all ease-in-out .3s;
            display: block;
        }
        #loginform input[type="submit"]:hover {
            background-color: #fff;
            color:  #0D324C;
            text-shadow: none;
        }
        #login form p.submit {
            text-align: center;
        }
        .login.wp-core-ui .button.button-large {
            height: auto;
        }
        .login form .forgetmenot {
            float: none !important;
            text-align: center;
        }
        .login .button-primary {
            float: none !important;
        }
        #login h1 a, .login h1 a {
            background-image: url(<?php echo get_stylesheet_directory_uri(); ?>/dist/images/fomaco_logo.svg);
            height:125px;
            max-width:620px;
            width: 100%;
            background-size: 620px 125px;
            background-repeat: no-repeat;
            padding-bottom: 30px;
        }
        .login #backtoblog,
        .login #nav {
            text-align: center;
            color: #fff !important;
        }
        .login #backtoblog a,
        .login #nav a {
            color: #fff !important;
            text-shadow: 0 1px 3px rgba(0, 0, 0, 1) !important;
        }
 
    </style>
 
<?php }
add_action( 'login_enqueue_scripts', 'my_login_logo' );

?>